# vis-inotify - watch files using inotifywait

## Example

I use this plugin to dynamically reload my vis theme if it has changed.

```lua
local theme = "theme"
local inotify = require('plugins/vis-inotify')
inotify.add_recreate_watch('/home/muhq/.config/vis/themes/theme.lua', function(msg)
  vis:command("set theme " .. theme) -- reload vis theme
end, '-P')
